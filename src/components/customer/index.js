const ApplyComponent = () => ({ component: import("./apply-component"), delay: 1000 });
const ApplicationFormComponent = () => import("./application-form");
const RetailAcademyComponent = () => import("./retail-academy");
const GrocerClubComponent = () => import("./grocer-club");

export {
    ApplicationFormComponent,
    ApplyComponent,
    RetailAcademyComponent,
    GrocerClubComponent
};
