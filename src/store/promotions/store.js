/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        promotions: [],
        promoItems: [],
        promotion: {},
        isLoading: false,
        itemsIsloading: false
    },
    getters: {
        isLoading: state => {
            return state.isLoading;
        },
        itemsIsloading: state => {
            return state.itemsIsloading;
        },
        promotions: state => {
            return state.promotions;
        },
        promotion: state => {
            return state.promotion;
        },
        promoItems: state => {
            return state.promoItems;
        }
    },
    actions: {
        async getPromotions({ commit, state }, payload) {
            try {
                state.isLoading = true;
                let uri = payload ? `api/promo/get/${payload}` : "api/promo/get";
                const token = !_.isEmpty(JSON.parse(localStorage.getItem(Auth.Personal))) ? Auth.Personal : Auth.Client;
                const response = await Methods.apiCall("GET", uri, {}, token);
                if (response.data.success) {
                    let mutations = _.isEmpty(payload) ? "setPromotions" : "setPromotion";
                    commit(mutations, response.data);
                }
            } catch (error) {

            }
            state.isLoading = false;
        },

        async getFeaturedPromotions({ commit, state }, payload) {
            try {
                state.isLoading = true;
                let uri = payload ? `api/promo/get/${payload}?featured=1` : "apipromo/get?featured=1";
                const token = !_.isEmpty(JSON.parse(localStorage.getItem(Auth.Personal))) ? Auth.Personal : Auth.Client;
                const response = await Methods.apiCall("GET", uri, {}, token);
                if (response.data.success) {
                    let mutations = _.isEmpty(payload) ? "setPromotions" : "setPromotion";
                    commit(mutations, response.data);
                }
            } catch (error) {

            }
            state.isLoading = false;
        },

        async getPromotionItems({ commit, state }, payload) {
            try {
                state.itemsIsloading = true;
                state.promoItems = {};
                const token = !_.isEmpty(JSON.parse(localStorage.getItem(Auth.Personal))) ? Auth.Personal : Auth.Client;
                const response = await Methods.apiCall("GET", `api/promo/get/${payload.promo_id}/items`, payload.query, token);
                if (response.data.success) {
                    commit("setPromoItems", response);
                }
            } catch (error) {

            }

            state.itemsIsloading = false;
        }
    },
    mutations: {
        setPromotions(state, payload) {
            state.promotions = payload.data;
        },
        setPromotion(state, payload) {
            state.promotion = payload.data;
        },
        setPromoItems(state, payload) {
            state.promoItems = payload.data;
        },
        updatePromotionItems(state, payload) {
            state.promoItems.data = _.map(state.promoItems.data, (product) => {
                if (product.item_code === payload.item_code) {
                    product.item.favorite = payload.favorite;
                    product.favorite = payload.favorite;
                }
                return product;
            });
        }
    }
};
