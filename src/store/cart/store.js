/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
import { Toast } from "@interfaces/swal";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        cartItems: [],
        cart: {},
        productLoading: {},
        cancelSources: {},
        isSuccess: false,
        itemCode: ""
    },
    getters: {
        cartItems: state => {
            return state.cartItems;
        },
        cart: state => {
            return state.cart;
        },
        itemCode: state => {
            return state.itemCode;
        },
        isLoading: state => {
            return state.isLoading;
        },
        isSuccess: state => {
            return state.isSuccess;
        },
        productLoading: state => {
            return state.productLoading;
        }
    },
    actions: {

        async getCartItems({ state, commit }, payload) {
            // state.isLoading = true;
            Methods.checkingApiRequest(state, "getBranches");
            // const cancelToken = state.cancelSources.getCartItems;
            try {
                const response = await Methods.apiCall("GET", "api/cart/get", payload, Auth.Personal);
                if (response.data.success) {
                    commit("setCartItems", response.data);
                }
            } catch (error) {

            }

            // state.isLoading = false;
        },
        async addToCart({ state, dispatch }, payload) {
            try {
                state.isLoading = true;
                state.isSuccess = false;
                const response = await Methods.apiCall("POST", "/api/cart/item/add", payload, Auth.Personal);

                if (response.data.success) {
                    Toast.fire({
                        icon: "success",
                        title:
                            "New item added to cart."

                    });
                    state.itemCode = payload.item_code;
                    dispatch("getCartItems", {});
                    state.isSuccess = true;
                }
                state.isLoading = false;
                return response.data;
            } catch (error) {

            }
        },

        async removeToCart({ state, dispatch }, payload) {
            state.isSuccess = false;
            try {
                const response = await Methods.apiCall("POST", `/api/cart/item/remove/${payload}`, { ref: payload }, Auth.Personal);

                if (response.data.success) {
                    state.isSuccess = true;
                    dispatch("getCartItems", {});
                }
                Methods.toastMessage(response.data);
            } catch (error) {

            }

            // state.isLoading = false;
        },
        async updateCartItem({ state, dispatch, commit }, payload) {
            Methods.checkingApiRequest(state, "updateCartItem");
            state.isLoading = true;
            state.isSuccess = false;

            try {
                const response = await Methods.apiCall("POST", `/api/cart/item/update/${payload.ref}`, payload, Auth.Personal);

                if (response.data.success) {
                    state.isSuccess = true;
                    if (payload.refresh_cart) {
                        dispatch("getCartItems", {});
                    } else {
                        commit("setUpdatedItem", response.data.cart_item);
                    }
                }
            } catch (error) {
            }
            // state.productLoading[payload.ref] = false;
            state.isLoading = false;
        },

        async clearCart({ state, commit }) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "clearCart");

            try {
                const response = await Methods.apiCall("POST", "api/cart/clear", {}, Auth.Personal);

                // Methods.toastMessage(response.data);
                if (response.data.success) {
                    state.cartItems = [];
                    state.cart = {};
                }
                state.isLoading = false;
                return response;
            } catch (error) {

            }

            state.isLoading = false;
        },
        async checkoutCart({ state, dispatch }, payload) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "checkoutCart");
            const cancelToken = state.cancelSources.checkoutCart;
            try {
                const response = await Methods.apiCall("POST", "/api/order/checkout", payload, Auth.Personal, cancelToken.token);
                state.isLoading = false;
                return response;
            } catch (error) {
            }

            state.isLoading = false;
        },
        async updateCart({ state, commit }, payload) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "updateCart");
            // const cancelToken = state.cancelSources.removeToCart;
            try {
                const response = await Methods.apiCall("POST", `/api/cart/update/${payload.cart_ref}`, payload, Auth.Personal);
                if (payload.showToast) {
                    Methods.toastMessage(response.data);
                }
                response.data.data = response.data.cart;
                if (response.data.success) {
                    commit("setCartItems", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        }
    },
    mutations: {
        setCartItems(state, payload) {
            // state.cart = payload.data.items payload.data;
            state.cart = !_.isEmpty(payload.data.items) ? payload.data : {};
            if (!_.isEmpty(state.itemCode)) {
                state.cartItems = _.map(payload.data.items, item => {
                    item.focus = item.item_code === state.itemCode;
                    return item;
                });
            } else {
                state.cartItems = payload.data.items;
            }
        },
        setProductLoading(state, payload) {
            state.productLoading[payload.key] = payload.value;
        },
        setCartDataItems(state, payload) {
            state.cartItems = payload;
        },
        setUpdatedList(state, ref) {
            state.cartItems = _.filter(state.cartItems, (item) => {
                return item.ref !== ref;
            });
        },
        setUpdatedItem(state, updatedItem) {
            state.cartItems = _.map(state.cartItems, (item) => {
                if (item.id === updatedItem.id) {
                    item = updatedItem;
                }
                return item;
            });
        }
    }
};
