import {
    BrandsComponent,
    PromotionComponent, PromotionDetailsComponent,
    ServicesComponent,
    CatalogueComponent,
    RouterViewComponent
} from "@components/index";

const ProductRoutes = [
    {
        path: "/products",
        component: RouterViewComponent,
        children: [
            { path: "/", component: BrandsComponent, name: "brands" },
            { path: ":branchCode?", component: BrandsComponent, name: "brands-branch", props: true },
            { path: ":branchCode/:parentCategory", component: BrandsComponent, name: "brands-category", props: true }
        ]
    },
    {
        path: "/promotions",
        component: RouterViewComponent,
        children: [
            { path: "/", component: PromotionComponent, name: "promotions" },
            { path: ":promotionId/details", component: PromotionDetailsComponent, name: "promotions-details", props: true }
        ]
    },
    { path: "/services", component: ServicesComponent, name: "services" },
    { path: "/catalogue", component: CatalogueComponent, name: "catalogue" }
];

export default ProductRoutes;
