/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        orderHistory: [],
        purchaseOrders: [],
        order: {},
        isLoading: false,
        gettingOrders: false,
        cancelSources: {}
    },
    getters: {
        orderHistory: state => {
            return state.orderHistory;
        },
        purchaseOrders: state => {
            return state.purchaseOrders;
        },
        order: state => {
            return state.order;
        },
        isLoading: state => {
            return state.isLoading;
        },
        gettingOrders: state => {
            return state.gettingOrders;
        }
    },
    actions: {
        async getOrderHistory({ commit, state }, payload) {
            try {
                state.isLoading = true;
                const response = await Methods.apiCall("GET", "api/auth/user/purchase-history", payload, Auth.Personal);
                if (response.data.success) {
                    commit("setOrderHistory", response.data);
                }
            } catch (error) {

            }
            state.isLoading = false;
        },
        async getOrder({ commit }, payload) {
            try {
                const response = await Methods.apiCall("GET", `api/auth/user/order/${payload}/items`, {}, Auth.Personal);
                if (response.data.success) {
                    commit("setOrder", response.data);
                }
            } catch (error) {

            }
        },
        async getUserPurchasePrders({ commit, state }, payload) {
            state.gettingOrders = true;
            let cancel = 0;
            Methods.checkingApiRequest(state, "getUserPurchasePrders");
            const cancelToken = state.cancelSources.getUserPurchasePrders;
            try {
                const response = await Methods.apiCall("GET", `/api/auth/user/purchase-history/${payload}`, {}, Auth.Personal, cancelToken.token);
                if ((response.data.success)) {
                    commit("setPurchaseOrders", response.data);
                }
            } catch (error) {
                cancel++;
            }
            if (!cancel) {
                state.gettingOrders = false;
            }
        }
    },
    mutations: {
        setOrderHistory(state, payload) {
            state.orderHistory = payload.data;
        },
        setOrder(state, payload) {
            state.order = payload.data;
        },
        setPurchaseOrders(state, payload) {
            state.purchaseOrders = payload.data;
        }
    }
};
