/* eslint-disable camelcase */
/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
import router from "@routes/index";
export default {
    namespaced: true,
    state: {
        templates: [],
        isLoading: false
    },
    getters: {
        isLoading: state => {
            return state.isLoading;
        },
        templates: state => {
            return state.templates;
        }
    },
    actions: {

        async getTemplates({ commit, state }, payload) {
            state.isLoading = true;
            try {
                const response = await Methods.apiCall("GET", "/api/auth/user/saved-templates", payload, Auth.Personal);
                if (response.data.success) {
                    commit("setTemplates", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        },
        async useTemplate({ commit, state }, payload) {
            state.isLoading = true;
            try {
                const response = await Methods.apiCall("POST", `/api/auth/user/template/${payload.temp_ref}/use`, payload, Auth.Personal);
                if (response.data.success) {
                    // commit("setTemplates", response.data);
                    Methods.toastMessage(response.data);
                    router.push({
                        name: "transaction-summary"
                    });
                }
            } catch (error) {

            }

            state.isLoading = false;
        },

        async deleteTemplate({ commit, state }, temp_ref) {
            state.isLoading = true;
            try {
                const response = await Methods.apiCall("POST", `/api/template/${temp_ref}/delete`, {}, Auth.Personal);

                if (response.data.success) {
                    Methods.toastMessage(response.data);
                    commit("setUpdatedList", temp_ref);
                };
            } catch (error) {
                console.log(error);
            }

            state.isLoading = false;
        },

        async saveTemplate({ commit, state }, payload) {
            state.isLoading = true;
            try {
                const response = await Methods.apiCall("POST", "/api/cart/saveas", payload, Auth.Personal);
                state.isLoading = false;
                return response.data;
            } catch (error) {

            }
        }
    },
    mutations: {
        setTemplates(state, payload) {
            state.templates = payload.data;
        },
        setUpdatedList(state, payload) {
            state.templates = _.filter(state.templates, template => {
                return template.template_ref !== payload;
            });
        }
    }
};
