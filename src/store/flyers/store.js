/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        flyers: [],
        flyer: {},
        cancelSources: {}
    },
    getters: {
        flyers: state => {
            return state.flyers;
        },
        flyer: state => {
            return state.flyer;
        },
        isLoading: state => {
            return state.isLoading;
        }
    },
    actions: {

        async getFlyers({ state, commit }, payload) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "getFlyers");
            try {
                let uri = payload ? `api/flyer/get/${payload}` : "api/flyer/get";
                const response = await Methods.apiCall("GET", uri, {}, Auth.Client);
                if (response.data.success) {
                    let mutations = _.isEmpty(payload) ? "setFlyers" : "setFlyer";
                    commit(mutations, response.data);
                }
            } catch (error) {
                // console.log(error);
            }

            state.isLoading = false;
        }
    },
    mutations: {
        setFlyers(state, payload) {
            state.flyers = payload.data;
        },
        setFlyer(state, payload) {
            state.flyer = payload.data;
        }
    }
};
