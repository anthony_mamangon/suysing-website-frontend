/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        faqs: {},
        cancelSources: {}
    },
    getters: {
        faqs: state => {
            return state.faqs;
        },
        isLoading: state => {
            return state.isLoading;
        }
    },
    actions: {

        async getFaqs({ state, commit }, payload) {
            state.isLoading = false;
            Methods.checkingApiRequest(state, "getFaqs");
            try {
                const response = await Methods.apiCall("GET", "api/faq/get", payload, Auth.Client);
                if (response.data.success) {
                    commit("setFaqs", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        }
    },
    mutations: {
        setFaqs(state, payload) {
            state.faqs = payload.data;
        }
    }
};
