const BrandsComponent = () => import("./brands");
const PromotionComponent = () => import("./promotion");
const ServicesComponent = () => import("./services");
const CatalogueComponent = () => import("./catalogue");

const PromotionListComponent = () => ({ component: import("./promotion-list"), delay: 1000 });
const PromotionFeaturedListComponent = () => ({ component: import("./promotion-list-featured"), delay: 1000 });
const PromotionDetailsComponent = () => ({ component: import("./promotion-details"), delay: 1000 });
const PromotionTilesComponent = () => ({ component: import("./promotion-tiles"), delay: 1000 });
export {
    BrandsComponent,
    PromotionComponent,
    ServicesComponent,
    CatalogueComponent,
    PromotionListComponent,
    PromotionFeaturedListComponent,
    PromotionTilesComponent,
    PromotionDetailsComponent
};
