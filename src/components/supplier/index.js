const ApplySupplierComponent = () => import("./apply-supplier");
const SupplierFormComponent = () => import("./supplier-form");

export {
    ApplySupplierComponent,
    SupplierFormComponent
};
