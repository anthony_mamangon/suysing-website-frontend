module.exports = {
  important: true,
  theme: {
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      'xxl': '1440px',
    },
    fontFamily: {
      display: ['Montserrat', 'sans-serif'],
      body: ['Roboto', 'sans-serif'],
    },
    fontWeight: {
      normal: 400,
      medium: 500,
      bold: 700,
    },
    extend: {
      colors: {
        pippin: '#FFE7E6',
        red: '#D90429',
        blue: '#F5FAFF',
        limeade: '#45B200',
        purple: {
            '500': '#1E009B',
            '550': '#000860',
            '600': '#00034C'
        },
        orange: {
          '100': '#fefbec',
          '200': '#ffc935',
          '300': '#FFB133',
          '400': '#FFAE22',
          '500': '#FF9D00'
        },
        yellow: '#FFFBEC',
        gray: {
            '100': '#F4F4F4',
            '200': '#D9D9D9',
            '500': '#A7A7A7',
            '600': '#A5A5A5',
            '700': '#9F9F9F',
            '800': '#707070',
            '900': '#4A4A4A'
        }
      },
      fontSize: {
        '7xl': '5rem',
        '8xl': '6rem',
        '9xl': '7rem',
      },
    //   margin: {
    //     '96': '24rem',
    //     '128': '32rem',
    //   },
    }
  },
//   variants: {
//     opacity: ['responsive', 'hover']
//   }
}