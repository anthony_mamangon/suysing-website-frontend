import { BranchComponent } from "../../components/index";

const BranchRoutes = [
    { path: "/branches", component: BranchComponent, name: "branch" }
];

export default BranchRoutes;
