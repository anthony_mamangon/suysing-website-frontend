
const FlyersListComponent = () => ({ component: import("./flyers-list"), delay: 1000 });
const FlyersDetailComponent = () => ({ component: import("./flyers-detail"), delay: 1000 });
export {
    FlyersListComponent,
    FlyersDetailComponent
};
