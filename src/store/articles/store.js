/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        articles: [],
        article: {},
        isLoading: false
    },
    getters: {
        isLoading: state => {
            return state.isLoading;
        },
        articles: state => {
            return state.articles;
        },
        article: state => {
            return state.article;
        }
    },
    actions: {
        async getArticles({ commit, state }, payload) {
            try {
                state.isLoading = true;
                const response = await Methods.apiCall("GET", "api/post/get", payload, Auth.Client);
                if (response.data.success) {
                    commit("setArticles", response.data);
                }
            } catch (error) {
                // console.log(error);
            }
            state.isLoading = false;
        },

        async getArticle({ commit, state }, payload) {
            try {
                state.isLoading = true;
                const response = await Methods.apiCall("GET", "api/post/get/" + payload, {}, Auth.Client);
                if (response.data.success) {
                    commit("setArticle", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        }
    },
    mutations: {
        setArticles(state, payload) {
            state.articles = payload.data;
        },
        setArticle(state, payload) {
            state.article = payload.data;
        }
    }
};
