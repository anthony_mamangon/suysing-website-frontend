const AboutComponent = () => ({ component: import("./about"), delay: 1000 });
const NewsComponent = () => ({ component: import("./news"), delay: 1000 }); import("./news.vue");
const NewsDetailsComponent = () => ({ component: import("./news-details"), delay: 1000 });
const NewsListComponent = () => ({ component: import("./news-list"), delay: 1000 });

export {
    AboutComponent,
    NewsComponent,
    NewsDetailsComponent,
    NewsListComponent
};
