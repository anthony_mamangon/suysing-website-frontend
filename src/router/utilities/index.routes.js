import {
    SearchResultsComponent,
    EmptyResultsComponent
} from "@components/index";

import { RouterViewComponent } from "@components/common";
const UtilitiesRoutes = [
    {
        path: "/search/:keyword",
        component: RouterViewComponent,
        children: [
            {
                path: "result", component: SearchResultsComponent, name: "search-result", props: true
            },
            {
                path: "empty-result", component: EmptyResultsComponent, name: "search-result-empty", props: true
            }

        ]
    }
];

export default UtilitiesRoutes;
