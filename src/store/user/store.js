/* eslint-disable camelcase */
/* eslint-disable no-undef */
import { Auth, PermissionNames } from "@interfaces/enum";
import router from "@routes/index";
import { Toast } from "@interfaces/swal";
export default {
    namespaced: true,
    state: {
        client_token: {},
        cancelSources: {},
        personal_token: {},
        accountInfo: {},
        orderDetails: {},
        isLoading: false,
        logged_in: false
    },
    getters: {
        client_token: state => {
            return state.client_token;
        },
        personal_token: state => {
            return state.personal_token;
        },
        orderDetails: state => {
            return state.orderDetails;
        },
        accountInfo: state => {
            return state.accountInfo;
        },
        isLoading: state => {
            return state.isLoading;
        },
        logged_in: state => {
            return state.logged_in;
        }
    },
    actions: {

        async getClientToken({ commit }) {
            if (_.isEmpty(localStorage.suysing_client_token)) {
                try {
                    const params = {
                        grant_type: "client_credentials",
                        client_id: process.env.APP_API_CLIENT_ID,
                        client_secret: process.env.APP_API_CLIENT_SECRET,
                        scope: "*"
                    };
                    const response = await Methods.apiCall("POST", "oauth/token", params);

                    if (response.data.access_token) {
                        commit("setClientToken", response.data);
                    }
                } catch (error) {

                }
            }
        },
        async getPersonalToken({ state, commit, dispatch }, payload) {
            state.isLoading = true;
            if (_.isEmpty(localStorage.personal_token)) {
                try {
                    const response = await Methods.apiCall("POST", "api/auth/login", payload, Auth.Client);
                    console.log(response);
                    if (response.data.success) {
                        Toast.fire({
                            icon: "success",
                            title:
                                "Logged in successfully."
                        });
                        await commit("setPersonalToken", response.data.user);

                        // const oldBranchCode = router.currentRoute.params.branchCode;
                        // const newBranchCode = response.data.user.info.default_branch_code;

                        let branchCode = "";
                        const {
                            cart,
                            ship_tos,
                            default_ship_to_id,
                            default_branch_code
                        } = response.data.user.info;

                        branchCode = default_branch_code;
                        if (!_.isEmpty(cart)) {
                            if (cart.transaction_type === "DEL") {
                                if (!_.isEmpty(ship_tos)) {
                                    const address = _.find(ship_tos, {
                                        ship_to_id: default_ship_to_id
                                    });
                                    if (!_.isEmpty(address)) {
                                        branchCode = address.branch_code;
                                    }
                                }
                            } else {
                                branchCode = cart.branch_code;
                            }
                        }
                        if (Methods.userHasPermission(PermissionNames.PlaceOrderOnline)) {
                            dispatch("CART_STORE/getCartItems", { branchCode }, { root: true });
                        }

                        window.location.href = "/products/" + branchCode;
                    } else {
                        Methods.toastMessage(response.data);
                        state.isLoading = false;
                        return false;
                    }
                } catch (error) {
                    console.log(error);
                }
            }
            state.isLoading = false;
        },
        async savedTemplates({ state, commit }) {
            state.isLoading = true;
            try {
                const response = await Methods.apiCall("GET", "api/auth/user/saved-templates", {}, Auth.Personal);

                if (response.data.access_token) {
                    // commit("setClientToken", response.data);
                }
            } catch (error) {

            }
            state.isLoading = true;
        },
        async getTemplates({ state, commit }, payload) {
            state.isLoading = true;
            try {
                // type, [use, items]
                const response = await Methods.apiCall("GET", `api/auth/user/template/${payload.load}/${payload.type}`, {}, Auth.Personal);

                if (response.data.access_token) {
                    // commit("setClientToken", response.data);
                }
            } catch (error) {

            }
            state.isLoading = true;
        },
        async activateSuccessCard({ state }, payload) {
            // state.isLoading = true;
            try {
                // type, [use, items]
                const response = await Methods.apiCall("POST", `/api/auth/user/success-card/${payload}/activate`, {}, Auth.Personal);
                // state.isLoading = false;
                return response;
            } catch (error) {

            }
            // state.isLoading = true;
        },
        async userPasswordEmail({ state, commit }, payload) {
            state.isLoading = true;
            try {
                // type, [use, items]
                const response = await Methods.apiCall("POST", "api/auth/password/email", payload, Auth.Client);

                if (response.data.access_token) {
                    // commit("setClientToken", response.data);
                }
            } catch (error) {

            }
            state.isLoading = true;
        },

        async getAccountInfo({ state, commit }) {
            state.isLoading = true;
            try {
                // type, [use, items]
                const response = await Methods.apiCall("GET", "api/auth/user/info", {}, Auth.Personal);
                if (response.data.success) {
                    commit("setAccountInfo", response.data);
                }
            } catch (error) {

            }
            state.isLoading = false;
        },

        async getOrderHistoryDetails({ state, commit, dispatch }, payload) {
            state.isLoading = true;
            try {
                // type, [use, items]
                const response = await Methods.apiCall("GET", `api/auth/user/purchase-history/${payload}`, {}, Auth.Personal);
                if (response.data.success) {
                    commit("setOrderDetails", response.data);
                    // dispatch("getOrderHistoryDetailItems", payload);
                }
            } catch (error) {
                // console.log(error);
            };
            state.isLoading = false;
        },

        async getOrderHistoryDetailItems({ state, commit }, payload) {
            state.isLoading = true;
            try {
                // type, [use, items]
                const response = await Methods.apiCall("GET", `api/auth/user/order/${payload}/items`, {}, Auth.Personal);
                if (response.data.success) {
                    commit("setOrderDetailItems", response.data);
                }
            } catch (error) {
                // console.log(error);
            }
            state.isLoading = false;
        },

        savePersonalToken({ commit }, payload) {
            commit("setPersonalToken", payload);
        },
        logoutUser({ state }) {
            localStorage.removeItem(Auth.Personal);
            state.personal_token = {};
            state.logged_in = false;
            if (router.currentRoute.name !== "Home") {
                router.push({
                    name: "Home"
                });
            }
        },
        async changePassword({ state, dispatch }, payload) {
            state.isLoading = true;
            try {
                const response = await Methods.apiCall("POST", "api/auth/user/password/change", payload, Auth.Personal);
                const { success, message, errors } = response.data;
                if (success) {
                    Swal.fire({
                        icon: "success",
                        title: "Success!",
                        text: message,
                        width: 400
                    }).then((result) => {
                        if (result.value) {
                            dispatch("logoutUser");
                        }
                    });
                } else {
                    // console.log(errors);
                    Methods.errorMessage(errors);
                }
            } catch (error) {
                // console.log(error.response);
                // Methods.errorMessage(error.response.data.errors);
            }
            state.isLoading = false;
        }
    },
    mutations: {
        setClientToken(state, payload) {
            var now = new Date();
            payload.expiration_date = new Date(now.getTime() + (payload.expires_in * 1000));
            state.client_token = payload;
            localStorage[Auth.Client] = JSON.stringify(payload);
        },
        setAccountInfo(state, payload) {
            state.accountInfo = payload.data;
        },
        setOrderDetails(state, payload) {
            state.orderDetails = payload.data;
        },
        setOrderDetailItems(state, payload) {
            state.orderDetails.items = payload.data;
        },
        setPersonalToken(state, payload) {
            payload.expiration_date = payload.expires_at;
            localStorage[Auth.Personal] = JSON.stringify(payload);
            state.personal_token = payload;
            state.logged_in = true;
        }
    }
};
