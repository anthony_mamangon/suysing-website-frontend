// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "@routes/index";
import store from "@stores/index.store";
import _ from "@interfaces/lodash";
import Methods from "@interfaces/methods";
import "@stylesheets/index.css";
import Mixins from "@interfaces/mixin";
import "@interfaces/filters";
import "@interfaces/directives";
import $ from "jquery";

// import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "bootstrap-vue/dist/bootstrap-vue-icons.min.css";

import Swal from "sweetalert2";

import "@assets/js/index.js";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueMask from "v-mask";
import VueNumeric from "vue-numeric";

window._ = _;
window.$ = $;
window.Methods = Methods;
window.Swal = Swal;
Vue.set(Vue.prototype, "_", _);
Vue.config.productionTip = false;
Vue.mixin(Mixins);

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueMask);
Vue.use(VueNumeric);

/* eslint-disable no-new */
new Vue({
    el: "#app",
    router,
    store,
    components: { App },
    template: "<App/>"
});
