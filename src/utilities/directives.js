/* eslint-disable space-before-function-paren */
import Vue from "vue";

Vue.directive("focus", {
    // When the bound element is inserted into the DOM...
    inserted: function (el, binding) {
        // Focus the element
        console.log(el.focus());
        if (binding.value) {
            el.focus();
        }
    }
});
