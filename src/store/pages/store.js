/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        pages: {},
        cancelSources: {}
    },
    getters: {
        pages: state => {
            return state.pages;
        },
        isLoading: state => {
            return state.isLoading;
        }
    },
    actions: {

        async getPageContent({ state, commit }, payload) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "getPageContent");
            try {
                // const cancelToken = state.cancelSources.getBranches;
                const response = await Methods.apiCall("GET", `/api/page/get/${payload}`, {}, Auth.Client);
                if (response.data.success) {
                    commit("setPages", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        }
    },
    mutations: {
        setPages(state, payload) {
            state.pages[payload.data.slug] = payload.data;
        }
    }
};
