/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        categories: [],
        cancelSources: {}
    },
    getters: {
        categories: state => {
            return state.categories;
        },
        isLoading: state => {
            return state.isLoading;
        }
    },
    actions: {

        async getCategories({ state, commit }, payload) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "getCategories");
            // const cancelToken = state.cancelSources.getCategories;
            try {
                const response = await Methods.apiCall("GET", "api/item/category/get", payload, Auth.Client);
                if (response.data.success) {
                    commit("setCategories", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        }
    },
    mutations: {
        setCategories(state, payload) {
            state.categories = payload.data;
        }
    }
};
