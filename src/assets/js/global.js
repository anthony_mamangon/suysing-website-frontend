/* eslint-disable */

import $ from "jquery";
// import router from "@routes/index";

// $(document).on("click", "#nav-toggle", function() {
//     document.getElementById("nav-content").classList.toggle("hidden");
//     let navContent = document.getElementsByClassName("vsc-initialized");
//     for (var i = 0; i < navContent.length; i++) {
//         navContent[i].classList.toggle("open");
//     }
// });

// $(document).ready(function(){
//     if ( $("#nav-content").hasClass("hidden") ) {
//       $(".nav-container").addClass("sticky")
//     };
// });

// Hide Login Form when clicking a nav link
// $(document).on("click", "a", function() {
//     var loginForm = document.getElementById("loginForm");
//     if (loginForm) {
//         loginForm.removeAttribute("open");
//     }
// });

// Toggle Brand Filters on mobile
$(document).on("click", "#filters_toggle", function() {
    document.getElementById("brand-filter").classList.toggle("hidden");
});

// Toggle Nav on Mobile
// $(document).on("click", "#search-toggle", function() {
//     document.getElementById("mobile-search").classList.toggle("hidden");
// });

// $(document).on("click", ".router-link-exact-active", function() {
//     document.getElementById("nav-content", "shopping-cart").classList.toggle("hidden");
// });

$(document).on("click", "#hide_ticker", function() {
    document.getElementById("alert_ticker").classList.toggle("hidden");
});

$(function() {
    $("#businessFormat_dropr").change(function() {
        if ($(this).val() === "otherBusinessFormat") {
            $("#specifyBusinessFormat").show();
        } else {
            $("#specifyBusinessFormat").hide();
        }
    });
});

$(document).ready(function(e) {
  var offset = 500,
    offset_opacity = 1200,
    scroll_top_duration = 700,
    $back_to_top = $("#back2top");

  //hide or show the "back to top" link
  $(window).scroll(function() {
    ($(this).scrollTop() > offset) ? $back_to_top.addClass("visible"):
      $back_to_top.removeClass("visible is-fade-out");
    if ($(this).scrollTop() > offset_opacity) {
      $back_to_top.addClass("is-fade-out");
    }
  });

  //smooth scroll to top
  $back_to_top.on("click", function(event) {
    event.preventDefault();
    $("body,html").animate({
      scrollTop: 0,
    }, scroll_top_duration);
  });

});

// Product Page Quantity Counter
// $(document).ready(function() {
//     $('.minus').click(function () {
//         var $input = $(this).parent().find('input');
//         var count = parseInt($input.val()) - 1;
//         count = count < 1 ? 1 : count;
//         $input.val(count);
//         $input.change();
//         return false;
//     });
//     $('.plus').click(function () {
//         var $input = $(this).parent().find('input');
//         $input.val(parseInt($input.val()) + 1);
//         $input.change();
//         return false;
//     });
// });
// eslint-disable-next-line
