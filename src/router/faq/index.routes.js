import { FaqListComponent } from "@components/index";

const FaqRoutes = [
    { path: "/faqs", component: FaqListComponent, name: "faq-list" }
];

export default FaqRoutes;
