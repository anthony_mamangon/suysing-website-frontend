import {
    CheckoutComponent,
    OrderSummaryComponent
} from "@components/index";

const TransactionRoutes = [
    {
        path: "/order-summary", component: OrderSummaryComponent, name: "transaction-summary", meta: { auth: true }
    },
    {
        path: "/checkout", component: CheckoutComponent, name: "transaction-checkout", meta: { auth: true }
    }
];

export default TransactionRoutes;
