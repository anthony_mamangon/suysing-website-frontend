/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        banner: [],
        partner: []
    },
    getters: {
        banner: state => {
            return state.banner;
        },
        partner: state => {
            return state.partner;
        }
    },
    actions: {
        async getBanners({ commit }, payload) {
            try {
                const response = await Methods.apiCall("GET", `api/${payload}/get`, {}, Auth.Client);
                if (response.data.success) {
                    response.data.key = payload;
                    commit("setBanners", response.data);
                }
            } catch (error) {

            }
        }
    },
    mutations: {
        setBanners(state, payload) {
            state[payload.key] = payload.data;
        }
    }
};
