import { DashBoardComponent, RouterViewComponent, OrderHistoryDetailsComponent, ChangePasswordComponent } from "@components/index";

const ProfileRoutes = [
    {
        path: "/dashboard",
        component: RouterViewComponent,
        children: [
            { path: "/", component: DashBoardComponent, name: "dashboard" },
            { path: "order-history/:orderRef/details", component: OrderHistoryDetailsComponent, name: "order-history-details", props: true },
            { path: "profile/change-password", component: ChangePasswordComponent, name: "change-password" }
        ],
        meta: { auth: true }
    }
];

export default ProfileRoutes;
