/* eslint-disable no-undef */
import Vue from "vue";
import Vuex from "vuex";

import USER_STORE from "./user/store";

import BANNER_STORE from "@stores/banner/store";

import FAQ_STORE from "@stores/faq/store";

import CAREER_STORE from "@stores/career/store";

import TESTIMONIAL_STORE from "@stores/testimonial/store";

import ARTICLE_STORE from "@stores/articles/store";

import BRANCH_STORE from "@stores/branch/store";

import CATEGORY_STORE from "@stores/category/store";

import PRODUCT_STORE from "@stores/product/store";

import APPLICATION_STORE from "@stores/application/store";

import CART_STORE from "@stores/cart/store";

import PAGE_STORE from "@stores/pages/store";

import PROMOTION_STORE from "@stores/promotions/store";

import FLYERS_STORE from "@stores/flyers/store";

import OPTIONS_STORE from "@stores/options/store";

import UTILITIES_STORE from "@stores/utilities/store";

import TEMPLATES_STORE from "@stores/templates/store";

import ORDER_STORE from "@stores/order/store";

// configure Vuex for modules
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        USER_STORE,
        BANNER_STORE,
        FAQ_STORE,
        CAREER_STORE,
        TESTIMONIAL_STORE,
        ARTICLE_STORE,
        BRANCH_STORE,
        CATEGORY_STORE,
        PRODUCT_STORE,
        APPLICATION_STORE,
        CART_STORE,
        PAGE_STORE,
        PROMOTION_STORE,
        FLYERS_STORE,
        OPTIONS_STORE,
        UTILITIES_STORE,
        TEMPLATES_STORE,
        ORDER_STORE
    }

});
