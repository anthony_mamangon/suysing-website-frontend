/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        testimonials: {},
        cancelSources: {}
    },
    getters: {
        testimonials: state => {
            return state.testimonials;
        },
        isLoading: state => {
            return state.isLoading;
        }
    },
    actions: {

        async getTestimonials({ state, commit }, payload) {
            state.isLoading = false;
            Methods.checkingApiRequest(state, "getTestimonials");
            try {
                const response = await Methods.apiCall("GET", "api/testimonial/get", payload, Auth.Client);
                if (response.data.success) {
                    commit("setTestimonials", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        }
    },
    mutations: {
        setTestimonials(state, payload) {
            state.testimonials = payload.data;
        }
    }
};
