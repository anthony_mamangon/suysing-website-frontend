import Vue from "vue";

Vue.filter("capitalize", function(value) {
    if (!value) return "";
    var seperator = value.includes("/") ? "/" : " ";

    value = value
        .toLowerCase()
        .replace(/\s{2,}/g, " ")
        .trim()
        .split(seperator)
        .map(function(word) {
            return formatText(word);
        })
        .join(seperator);
    if (seperator === "/") {
        value = value
            .replace(/\s{2,}/g, " ")
            .trim()
            .split(" ")
            .map(function(word) {
                return formatText(word);
            })
            .join(" ");
    }
    return value;
});

function formatText(word) {
    if (word.length > 0) {
        return word[0].toUpperCase() + word.substr(1);
    }
    return word;
}
