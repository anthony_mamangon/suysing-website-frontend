import { FlyersListComponent } from "@components/index";

import { RouterViewComponent } from "@components/common/index";
const FlyersRoutes = [
    {
        path: "/flyers",
        component: RouterViewComponent,
        children: [
            {
                path: "/", component: FlyersListComponent, name: "flyers-list"
            },
            {
                path: ":flyerId/details", component: FlyersListComponent, name: "flyer-details", props: true
            }

        ]
    }
];

export default FlyersRoutes;
