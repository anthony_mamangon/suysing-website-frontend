
const ProductInfoModal = () => ({ component: import("./product-info-modal"), delay: 1000 });
const AddProductModal = () => ({ component: import("./add-product-modal"), delay: 1000 });
const SuccessCardModal = () => ({ component: import("./success-card-modal"), delay: 1000 });
const TermsModal = () => ({ component: import("./terms-modal"), delay: 1000 });
const BranchModal = () => ({ component: import("./branch-modal"), delay: 1000 });
const BranchSuccessModal = () => ({ component: import("./branch-modal"), delay: 1000 });
const RestrictionModal = () => ({ component: import("./restriction-modal"), delay: 1000 });
const TemplateModal = () => ({ component: import("./template-modal"), delay: 1000 });
const ClearCartModal = () => ({ component: import("./clear-cart-modal"), delay: 1000 });
const OrderSubmittedModal = () => ({ component: import("./order-submitted-modal"), delay: 1000 });
const CheckoutPromptModal = () => ({ component: import("./checkout-prompt-modal"), delay: 1000 });
const RemoveItemModal = () => ({ component: import("./remove-item-modal"), delay: 1000 });
const ActivateSuccessCardModal = () => ({ component: import("./activated-success-card-modal"), delay: 1000 });

export {
    ProductInfoModal,
    AddProductModal,
    SuccessCardModal,
    TermsModal,
    BranchModal,
    RestrictionModal,
    TemplateModal,
    ClearCartModal,
    OrderSubmittedModal,
    BranchSuccessModal,
    RemoveItemModal,
    ActivateSuccessCardModal,
    CheckoutPromptModal
};
