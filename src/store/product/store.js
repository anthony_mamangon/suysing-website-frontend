/* eslint-disable indent */
/* eslint-disable camelcase */
/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
import router from "@routes/index";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        products: [],
        filters: null,
        cancelSources: {},
        productsView: "grid",
        brandTilesHeight: 250,
        itemCodes: []
    },
    getters: {
        products: state => {
            return state.products;
        },
        itemCodes: state => {
            return state.itemCodes;
        },
        isLoading: state => {
            return state.isLoading;
        },
        productsView: state => {
            return state.productsView;
        },
        filters: state => {
            return state.filters;
        },
        brandTilesHeight: state => {
            return state.brandTilesHeight;
        }
    },
    actions: {

        async getProducts({ state, commit }, payload) {
            state.isLoading = true;
            state.products = {};
            let cancel = 0;
            Methods.checkingApiRequest(state, "getProducts");
            const cancelToken = state.cancelSources.getProducts;
            try {
                const token = !_.isEmpty(JSON.parse(localStorage.getItem(Auth.Personal))) ? Auth.Personal : Auth.Client;
                const response = await Methods.apiCall("GET", `/api/item/branch/${payload.branchCode}/get`, payload, token, cancelToken.token);
                if ((response.data.success)) {
                    commit("setProducts", response.data);
                }
            } catch (error) {
                cancel++;
            }
            if (!cancel) {
                state.isLoading = false;
            }
        },

        async getProductByCode({ state, commit }, itemCode) {
            state.isLoading = true;
            try {
                const response = await Methods.apiCall("GET", `api/item/get/${itemCode}`, {}, Auth.Client);
                state.isLoading = false;
                return response.data.data;
            } catch (error) {

            }
        },
        async getItemPromoByCode({ state }, payload) {
            try {
                const { branch_code, item_code, promoChecker, item_codes } = payload;

                // let url = `api/item/branch/${branch_code}/get/${item_code}`;
                let url = `api/item/branch/${branch_code}/get`;
                url = promoChecker ? `${url}/haspromo` : `${url}/${item_code}`;
                const method = promoChecker ? "POST" : "GET";
                const token = !_.isEmpty(JSON.parse(localStorage.getItem(Auth.Personal))) ? Auth.Personal : Auth.Client;
                const response = await Methods.apiCall(method, url, { item_codes }, token);
                state.itemCodes.push(item_code);

                return response.data;
            } catch (error) {
                console.log(error);
            }
        },
        async favoriteProduct({ state, commit, dispatch }, payload) {
            try {
                const { branch_code, item_code, favorite } = payload;
                let uri = "/api/item/favorite/";

                uri += !favorite ? `${item_code}/${branch_code}/delete` : "add";

                const response = await Methods.apiCall("POST", uri, payload, Auth.Personal);
                if (response.data.success) {
                    if (favorite) {
                        Methods.toastMessage(response.data);
                    }
                    switch (router.currentRoute.name) {
                        case "search-result":
                            commit("UTILITIES_STORE/updateSearchItems", payload, { root: true });
                            break;
                        case "promotions-details":
                            commit("PROMOTION_STORE/updatePromotionItems", payload, { root: true });
                            break;
                        default:
                            commit("updateProducts", payload);
                    }
                }
            } catch (error) {

            }
        }
    },
    mutations: {
        setProducts(state, payload) {
            state.products = payload;
            // state.products.data = _.filter(state.products.data, product => {
            //     return product.item.minimum > 1;
            // });
        },
        setProductsView(state, payload) {
            state.productsView = payload;
        },
        setBrandTilesHeight(state, payload) {
            state.brandTilesHeight = payload;
        },
        setFilters(state, payload) {
            if (payload.removing) {
                state.filters = _.filter(state.filters, i => {
                    return i !== payload.filter;
                });
                return;
            }
            state.filters = _.isEmpty(payload.filters) ? null : payload.filters;
        },
        updateProducts(state, payload) {
            state.products.data = _.map(state.products.data, (product) => {
                if (product.item_code === payload.item_code) {
                    product.item.favorite = payload.favorite;
                    product.favorite = payload.favorite;
                }
                return product;
            });
        }
    }
};
