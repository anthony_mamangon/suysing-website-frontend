/* eslint-disable space-before-function-paren */
/* eslint-disable no-undef */
import Vue from "vue";
import Router from "vue-router";
import { Home } from "@/components/index";
import { TermsInfoComponent } from "@/components/common/index";
import { Auth, AppName, PermissionNames } from "@interfaces/enum";
import AboutRoutes from "./about/index.routes";
import BranchRoutes from "./branch/index.routes";
import ProductRoutes from "./products/index.routes";
import CustomerRoutes from "./customer/index.routes";
import SupplierRoutes from "./supplier/index.routes";
import CareerRoutes from "./career/index.routes";
import FaqRoutes from "./faq/index.routes";
import ProfileRoutes from "./profile/index.routes";
import TransactionRoutes from "./transaction/index.routes";
import FlyersRoutes from "./flyers/index.routes";
import UtilitiesRoutes from "./utilities/index.routes";

Vue.use(Router);

const routes = new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "Home",
            component: Home
        },
        {
            path: "/terms/:slug",
            name: "terms-info",
            component: TermsInfoComponent,
            props: true
        },
        ...AboutRoutes,
        ...BranchRoutes,
        ...ProductRoutes,
        ...CustomerRoutes,
        ...SupplierRoutes,
        ...CareerRoutes,
        ...FaqRoutes,
        ...ProfileRoutes,
        ...TransactionRoutes,
        ...FlyersRoutes,
        ...UtilitiesRoutes
    ]
});

routes.beforeEach((to, from, next) => {
    let pathName = "";
    let path = _.split(_.trimStart(to.path, "/"), "/", 3);
    if (!_.isEmpty(path)) {
        _.forEach(path, (url) => {
            if (isNaN(url)) {
                let name = formatText(url);
                pathName += `${name} | `;
            }
        });
        pathName = _.trimEnd(pathName, " |");
    }

    const loginForm = document.getElementById("loginForm");
    if (loginForm) {
        loginForm.removeAttribute("open");
    }

    pathName = !_.isEmpty(pathName) ? pathName : "Home";
    document.title = `${AppName} | ${pathName}`;

    if (to.meta.auth) {
        if (!localStorage.getItem(Auth.Personal)) {
            next("/");
            // eslint-disable-next-line no-useless-return
            return;
        }
    }
    if (to.matched.some(record => to.meta.permission)) {
        if (to.meta.permission === "cart") {
            if (!Methods.userHasPermission(PermissionNames.ViewProductPrice)) {
                next("/");
                // eslint-disable-next-line no-useless-return
                return;
            }

            if (!Methods.userHasPermission(PermissionNames.PlaceOrderOnline)) {
                next("/");
                // eslint-disable-next-line no-useless-return
                return;
            }
        } else {
            if (!Methods.userHasPermission(to.meta.permission)) {
                next("/");
                // eslint-disable-next-line no-useless-return
                return;
            }
        }
    }
    window.scrollTo(0, 0);
    next();
});

function formatText(text) {
    if (!text) return "";
    return text
        .toLowerCase()
        .replace(/%20/g, " ")
        .split("-")
        .map(function (text) {
            return text[0].toUpperCase() + text.substr(1);
        })
        .join(" ");
}

export default routes;
