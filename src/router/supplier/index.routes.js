import { ApplySupplierComponent } from "@components/index";
import { RouterViewComponent } from "@components/common/index";

const SupplierRoutes = [
    {
        path: "/suppliers",
        component: RouterViewComponent,
        children: [
            {
                path: ":supplierType/", component: ApplySupplierComponent, name: "supplier-apply", props: true
            },
            {
                path: "form-submited", component: ApplySupplierComponent, name: "supplier-submitted"
            }

        ]
    }
];

export default SupplierRoutes;
