/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        careers: {},
        career: {},
        cancelSources: {},
        fieldsOfWork: []
    },
    getters: {
        careers: state => {
            return state.careers;
        },
        career: state => {
            return state.career;
        },
        fieldsOfWork: state => {
            return state.fieldsOfWork;
        },
        isLoading: state => {
            return state.isLoading;
        }
    },
    actions: {

        async getCareers({ state, commit }, payload) {
            state.isLoading = false;
            Methods.checkingApiRequest(state, "getCareers");
            try {
                const response = await Methods.apiCall("GET", "api/career/get", payload, Auth.Client);
                if (response.data.success) {
                    commit("setCareers", response.data);
                }
            } catch (error) {

            }
            state.isLoading = false;
        },
        async getCareer({ state, commit }, payload) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "getCareers");
            try {
                const response = await Methods.apiCall("GET", `api/career/get/${payload}`, {}, Auth.Client);
                if (response.data.success) {
                    commit("setCareer", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        },
        async filterCareer({ state, commit }, payload) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "filterCareer");
            try {
                const response = await Methods.apiCall("POST", "api/career/search", payload, Auth.Client);
                if (response.data.success) {
                    commit("setCareers", response.data);
                }
            } catch (error) {
            }

            state.isLoading = false;
        },
        async getFieldOfWorks({ state, commit }) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "getFieldOfWorks");
            try {
                const response = await Methods.apiCall("GET", "api/field_of_work/get", {}, Auth.Client);
                if (response.data.success) {
                    commit("setfieldsOfWork", response.data);
                }
            } catch (error) {
            }

            state.isLoading = false;
        }
    },
    mutations: {
        setCareers(state, payload) {
            state.careers = payload.data;
        },
        setCareer(state, payload) {
            state.career = payload.data[0];
        },
        setfieldsOfWork(state, payload) {
            state.fieldsOfWork = _.map(payload.data, (item) => {
                item.value = item.id;
                item.text = item.title;
                return item;
            });
        }
    }
};
