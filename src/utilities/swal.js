/* eslint-disable no-undef */
import Swal from "sweetalert2";
const Toast = Swal.mixin({
    toast: true,
    position: "center",
    showConfirmButton: false,
    timer: 3000
});

const errorMessage = function(response) {
    let message = "";
    _.forEach(response, function(data) {
        message += "<div class='alert alert-danger' role='alert'>";
        message += data[0];
        message += "</div>";
    });

    Swal.fire({
        title: "Error!",
        html: message,
        icon: "error",
        width: 400
    });
};

export {
    Toast,
    errorMessage
};
