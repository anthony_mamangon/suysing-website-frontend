import { CareerFormComponent, CareerListComponent } from "@components/index";

import { RouterViewComponent } from "@components/common/index";

const CareerRoutes = [
    { path: "/careers", component: CareerListComponent, name: "career-list" },
    {
        path: "/job-post-form/:id/:title",
        component: RouterViewComponent,
        children: [
            {
                path: "/", component: CareerFormComponent, name: "career-job-form", props: true
            },
            {
                path: "form-submited", component: CareerFormComponent, name: "career-submitted", props: true
            }
        ]
    }
];

export default CareerRoutes;
