/* eslint-disable no-undef */

import { Toast, errorMessage } from "@interfaces/swal";
import { Auth } from "@interfaces/enum";
import router from "@routes/index";
export default {
    namespaced: true,
    state: {
        application: {},
        isLoading: false
    },
    getters: {
        isLoading: state => {
            return state.isLoading;
        },
        application: state => {
            return state.application;
        }
    },
    actions: {
        async submitApplication({ state }, payload) {
            try {
                let agree = "";
                if (payload.type === "supplier" || payload.type === "job") {
                    agree = JSON.parse(payload.data.get("agree"));
                } else {
                    agree = payload.data.agree;
                }

                if (!agree) {
                    Toast.fire({
                        icon: "info",
                        title:
                            "Please agree to terms and conditions before you proceed"
                    });
                    // eslint-disable-next-line no-useless-return
                    return;
                }
                state.isLoading = true;
                if (payload.type !== "customer") {
                    payload.data.headers = {
                        "Content-Type": "multipart/form-data"
                    };
                }

                const response = await Methods.apiCall("POST", `/api/application/${payload.type}/add`, payload.data, Auth.Client);
                // Methods.SwalMessage(response.data);

                if (response && response.data && response.data.success) {
                    /* eslint-disable indent */
                    let name = "";
                    const params = { success: response.data.success };
                    switch (payload.type) {
                        case "supplier":
                            name = "supplier-submitted";
                            params.type = router.currentRoute.params.supplierType;
                            break;
                        case "customer":
                            name = "customer-submitted";
                            params.type = "be-a-customer";
                            break;
                        case "job":
                            name = "career-submitted";
                            params.type = payload.type;
                            break;
                    }
                    state.application = {};
                    router.replace({
                        name,
                        query: params
                    });
                }
            } catch (error) {
                console.log(error);
                errorMessage(error.response.data.errors);

                state.isLoading = false;
            }

            state.isLoading = false;
        }
    },
    mutations: {
        response(state, payload) {
            state.articles = payload.data;
        },
        setApplication(state, payload) {
            state.application = payload;
        }
    }
};
