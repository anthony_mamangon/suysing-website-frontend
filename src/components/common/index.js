import Vue from "vue";
// import VueCarousel from "@chenfengyuan/vue-carousel";
import VueCarousel from "@chenfengyuan/vue-carousel";
Vue.use(VueCarousel);

const SpinnerComponent = () => import("./spinner");

const MainNavComponent = () => ({ component: import("./main-nav"), loading: SpinnerComponent, delay: 1000 });
const MainNavLoggedInComponent = () => ({ component: import("./main-nav-logged-in.vue"), loading: SpinnerComponent, delay: 1000 });
const HeaderComponent = () => ({ component: import("./header"), loading: SpinnerComponent, delay: 1000 });
const AlertComponent = () => ({ component: import("./alert"), loading: SpinnerComponent, delay: 1000 });
const HeroComponent = () => ({ component: import("./hero"), loading: SpinnerComponent, delay: 1000 });
const HeroSmComponent = () => ({ component: import("./hero-sm"), loading: SpinnerComponent, delay: 1000 });
const HeroMdComponent = () => ({ component: import("./hero-md"), loading: SpinnerComponent, delay: 1000 });
const HeroLgComponent = () => ({ component: import("./hero-lg"), loading: SpinnerComponent, delay: 1000 });
const CardComponent = () => ({ component: import("./card"), loading: SpinnerComponent, delay: 1000 });
const FooterComponent = () => ({ component: import("./footer"), loading: SpinnerComponent, delay: 1000 });
const BrandTilesComponent = () => ({ component: import("./brand-tiles"), loading: SpinnerComponent, delay: 1000 });
const BrandFilterComponent = () => ({ component: import("./brand-filter"), loading: SpinnerComponent, delay: 1000 });
const BrandNavComponent = () => ({ component: import("./brand-nav"), loading: SpinnerComponent, delay: 1000 });
const RouterViewComponent = () => ({ component: import("./router-view-component"), loading: SpinnerComponent, delay: 1000 });
const CustomerReviewComponent = () => ({ component: import("./customer-review"), loading: SpinnerComponent, delay: 1000 });
const CarouselComponent = () => ({ component: import("./carousel"), loading: SpinnerComponent, delay: 1000 });
const PaginationComponent = () => ({ component: import("./pagination"), loading: SpinnerComponent, delay: 1000 });
const TermsInfoComponent = () => ({ component: import("./terms-info"), loading: SpinnerComponent, delay: 1000 });
const QuantityPickerComponent = () => ({ component: import("./quantity-picker"), loading: SpinnerComponent, delay: 1000 });
const ResponseMessageComponent = () => ({ component: import("./response-message"), loading: SpinnerComponent, delay: 1000 });
const CarouselThumbnailComponent = () => ({ component: import("./carousel-thumbnail"), loading: SpinnerComponent, delay: 1000 });
const ItemPromoListComponent = () => ({ component: import("./item-promo-list"), loading: SpinnerComponent, delay: 1000 });
const PromotionTilesComponent = () => ({ component: import("./promotion-tiles"), loading: SpinnerComponent, delay: 1000 });
const TabsComponent = () => ({ component: import("./tabs/tabs-component"), loading: SpinnerComponent, delay: 1000 });
const FilesUploaderComponent = () => ({ component: import("./files-uploader"), loading: SpinnerComponent, delay: 1000 });

// GLOBAL COMPONENT
Vue.component(
    "spinner-component",
    // The `import` function returns a Promise.
    () => import("./spinner")
);
export {
    MainNavComponent,
    MainNavLoggedInComponent,
    HeaderComponent,
    AlertComponent,
    HeroComponent,
    HeroSmComponent,
    HeroMdComponent,
    HeroLgComponent,
    SpinnerComponent,
    CardComponent,
    FooterComponent,
    BrandFilterComponent,
    BrandTilesComponent,
    BrandNavComponent,
    RouterViewComponent,
    CustomerReviewComponent,
    CarouselComponent,
    TermsInfoComponent,
    PaginationComponent,
    QuantityPickerComponent,
    ResponseMessageComponent,
    CarouselThumbnailComponent,
    ItemPromoListComponent,
    PromotionTilesComponent,
    FilesUploaderComponent,
    TabsComponent

};
