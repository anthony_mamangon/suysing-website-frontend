const DashBoardComponent = () => import("./dashboard");

const OrderHistoryDetailsComponent = () => ({ component: import("./order-history-details"), delay: 1000 });
const ChangePasswordComponent = () => ({ component: import("./change-password"), delay: 1000 });

export {
    DashBoardComponent,
    OrderHistoryDetailsComponent,
    ChangePasswordComponent
};
