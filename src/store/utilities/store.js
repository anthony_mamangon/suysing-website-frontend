/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
import { Toast } from "@interfaces/swal";
export default {
    namespaced: true,
    state: {
        searchResult: [],
        searchItemResult: [],
        isLoading: false,
        cancelSources: {},
        currentKeyword: ""
    },
    getters: {
        isLoading: state => {
            return state.isLoading;
        },
        searchResult: state => {
            return state.searchResult;
        },
        currentKeyword: state => {
            return state.currentKeyword;
        },
        searchItemResult: state => {
            return state.searchItemResult;
        },
        cancelSources: state => {
            return state.cancelSources;
        }
    },
    actions: {
        async onSearchByKeyword({ commit, state }, payload) {
            try {
                if (!_.isEmpty(payload.keyword) && payload.keyword.length < 2) {
                    Toast.fire({
                        icon: "info",
                        title: "Search key must be at least two characters."
                    });
                    state.searchResult = [];
                    return;
                }
                state.isLoading = true;
                state.searchResult = {};
                Methods.checkingApiRequest(state, "onSearchByKeyword");
                const cancelToken = state.cancelSources.onSearchByKeyword;
                const token = !_.isEmpty(JSON.parse(localStorage.getItem(Auth.Personal))) ? Auth.Personal : Auth.Client;
                const response = await Methods.apiCall("POST", `/api/util/search/${payload.keyword}`, payload, token, cancelToken.token);
                state.currentKeyword = payload.keyword;
                if (response.data.success) {
                    commit("setSearchResult", response.data);
                }
            } catch (error) {
                // console.log(error);
            }
            state.isLoading = false;
        },
        async onSearchItemsKeyword({ commit, state }, payload) {
            try {
                state.isLoading = true;
                Methods.checkingApiRequest(state, "onSearchItemsKeyword");
                const cancelToken = state.cancelSources.onSearchItemsKeyword;
                const response = await Methods.apiCall("POST", `/api/util/item/search/${payload.keyword}`, payload.data, Auth.Personal, cancelToken.token);
                if (!_.isEmpty(response.data) && response.data.success) {
                    commit("setSearchItemResult", response.data.date);
                    state.isLoading = false;
                }
            } catch (error) {
                console.log(error);
                state.isLoading = false;
            }
        }
    },
    mutations: {
        setSearchResult(state, payload) {
            state.searchResult = payload;
        },
        setSearchItemResult(state, payload) {
            state.searchItemResult = payload;
        },
        setCurrentKeyword(state, payload) {
            state.currentKeyword = payload;
        },
        setIsLoading(state, payload) {
            state.isLoading = payload;
        },
        updateSearchItems(state, payload) {
            state.searchResult.data = _.map(state.searchResult.data, (product) => {
                if (product.item_code === payload.item_code) {
                    product.item.favorite = payload.favorite;
                    product.favorite = payload.favorite;
                }
                return product;
            });
        }

    }
};
