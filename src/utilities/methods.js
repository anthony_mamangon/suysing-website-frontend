/* eslint-disable camelcase */
/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
import axios from "axios";
import _ from "@interfaces/lodash";
import store from "@stores/index.store";
import router from "@routes/index";
import { Auth } from "@interfaces/enum";
import { Toast } from "@interfaces/swal";
import Swal from "sweetalert2";

const apiCall = async function (METHOD = "GET", REQUEST_URI = "", PARAMS = {}, authorization = "", cancelToken = "") {
    try {
        const defaultHeaders = {
            "Accept": "application/json",
            "platform-type": "web",
            "app-version": "1.0.0",
            "app-name": process.env.APP_NAME
        };
        let headers = !_.isEmpty(PARAMS.headers) ? _.merge(PARAMS.headers, defaultHeaders) : defaultHeaders;

        const axiosInstance = axios.create({
            baseURL: process.env.APP_BASE_URL,
            headers
        });
        // CHECK FOR TOKEN EXPIRATION
        if (authorization) {
            let token_expired = false;
            let method = authorization === Auth.Personal ? "getPersonalToken" : "getClientToken";
            let token = JSON.parse(localStorage.getItem(authorization));

            if (!_.isEmpty(token)) {
                if (!_.isEmpty(token.expiration_date)) {
                    const now = new Date().getTime();
                    const expiration_date = new Date(token.expiration_date).getTime();
                    token_expired = now >= expiration_date;
                } else {
                    token_expired = true;
                }
            }

            if (_.isEmpty(token) || token_expired) {
                if (method === "getPersonalToken") {
                    localStorage.removeItem(authorization);
                    router.push({ name: "Home" });
                    return;
                }
                await store.dispatch("USER_STORE/getClientToken");
                token = JSON.parse(localStorage.getItem(authorization));
            }
            axiosInstance.defaults.headers.common["Authorization"] = "Bearer " + token.access_token;
        }
        METHOD = METHOD.toUpperCase();
        let response = {};
        switch (METHOD) {
            case "GET":
                response = await axiosInstance.get(REQUEST_URI, { params: PARAMS, cancelToken });
                break;
            case "POST":
                response = await axiosInstance.post(REQUEST_URI, PARAMS, { cancelToken });
                break;
            case "DELETE":
                response = await axiosInstance.delete(REQUEST_URI, { params: PARAMS });
                break;
        }
        return response;
    } catch (error) {
        if (axios.isCancel(error)) {
            return { success: false };
        }
        if (error.response) {
            if (error.response.statusText === "Unauthorized" || error.response.status === 401) {
                if (authorization === Auth.Personal) {
                    localStorage.removeItem(authorization);
                    location.reload();
                    return;
                }
                await store.dispatch("USER_STORE/getClientToken");
                location.reload();
                return;
            }

            if (!_.isEmpty(error.response.data.errors)) {
                await errorMessage(error.response.data.errors);
            }
        }
    }
};
const checkingApiRequest = function (state, method) {
    var cancelToken = state.cancelSources[method];
    if (!_.isEmpty(cancelToken)) {
        cancelToken.cancel();
    }
    state.cancelSources[method] = axios.CancelToken.source();
    cancelToken = axios.CancelToken.source();
};

const toastMessage = function toastMessage(message) {
    Toast.fire({
        icon: message.success ? "success" : "error",
        title: message.message
    });
};

const SwalMessage = function toastMessage(message) {
    Swal.fire({
        icon: message.success ? "success" : "error",
        title: message.success ? "Success" : "Error",
        text: message.message,
        width: 400
    });
};

const errorMessage = function (response, title = null) {
    let message = "";
    if (_.isObject(response)) {
        _.forEach(response, (data, key) => {
            message += "<div class='alert alert-danger' role='alert'>";
            message += _.isArray(data) ? _.join(data, " ") : data;
            message += "</div>";
        });
    } else {
        message += "<div class='alert alert-danger' role='alert'>";
        message += response;
        message += "</div>";
    }

    Swal.fire({
        title: title || "Error!",
        html: message,
        icon: "info",
        width: 400
    });
};
const userHasPermission = function (permissionName) {
    try {
        const personalInfo = store.state.USER_STORE.personal_token.info;
        if (_.isEmpty(permissionName) || !personalInfo) return false;
        if (_.isEmpty(personalInfo) && _.isEmpty(personalInfo.permissions)) return false;
        return _.includes(
            personalInfo.permissions,
            permissionName
        );
    } catch (error) {
        // console.log(error);
    }
};

const localStoreSupport = function () {
    try {
        let testKey = "test";
        let storage = window.sessionStorage;
        storage.setItem(testKey, "1");
        storage.removeItem(testKey);
        return true;
    } catch (error) {
        return false;
    }
};

const getCookieValue = function (name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) {
        let val = parts
            .pop()
            .split(";")
            .shift();
        return JSON.parse(decodeURIComponent(val));
    }
};

const formatNames = function (array, key, characters = []) {
    const formatedArray = _.map(array, i => {
        if (!_.isEmpty(i[key])) {
            let targetValue = _.lowerCase(i[key]);
            let mergeArray = _.concat(_.split(targetValue, " "), characters);

            let duplicate = checkIfDuplicateExists(mergeArray);
            if (!_.isEmpty(duplicate)) {
                duplicate = capitalize(duplicate.pop());
                targetValue = capitalize(targetValue);
                targetValue = _.replace(targetValue, duplicate, _.lowerCase(duplicate));
                i[key] = targetValue;
            }
        }

        return i;
    });
    return formatedArray;
};

function capitalize(value) {
    return value
        .toLowerCase()
        .split(" ")
        .map(function (word) {
            return word[0].toUpperCase() + word.substr(1);
        })
        .join(" ");
}

function validLength(value, length, trim = 3) {
    if (_.isEmpty(value)) return false;
    value = value
        .toLowerCase()
        .replace(/\s/g, "")
        .substring(trim)
        .trim();
    return length === value.length;
}

function checkIfDuplicateExists(array) {
    let duplicates = _.filter(array, (item, index) => {
        return _.indexOf(array, item) !== index;
    });

    return duplicates;
}

export default {
    apiCall,
    checkingApiRequest,
    toastMessage,
    SwalMessage,
    userHasPermission,
    localStoreSupport,
    getCookieValue,
    formatNames,
    errorMessage,
    validLength
};
