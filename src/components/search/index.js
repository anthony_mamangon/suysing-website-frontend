
const SearchResultsComponent = () => ({ component: import("./search-results"), delay: 1000 });
const EmptyResultsComponent = () => ({ component: import("./search-results-empty"), delay: 1000 });
export {
    SearchResultsComponent,
    EmptyResultsComponent
};
