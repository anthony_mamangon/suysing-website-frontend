/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        options: [],
        option: {},
        cancelSources: {}
    },
    getters: {
        options: state => {
            return state.options;
        },
        option: state => {
            return state.option;
        },
        isLoading: state => {
            return state.isLoading;
        }
    },
    actions: {

        async getOptions({ state, commit }, payload) {
            state.isLoading = false;
            Methods.checkingApiRequest(state, "getOptions");
            try {
                let uri = payload ? `api/option/get/${payload}` : "api/option/get";
                const response = await Methods.apiCall("GET", uri, payload, Auth.Client);
                if (response.data.success) {
                    let mutations = _.isEmpty(payload) ? "setOptions" : "setOption";
                    commit(mutations, response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        }
    },
    mutations: {
        setOptions(state, payload) {
            state.options = payload.data;
        },
        setOption(state, payload) {
            state.option = payload.data;
        }
    }
};
