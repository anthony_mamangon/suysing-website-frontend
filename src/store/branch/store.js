/* eslint-disable no-undef */
import { Auth } from "@interfaces/enum";
export default {
    namespaced: true,
    state: {
        isLoading: false,
        branches: [],
        cancelSources: {}
    },
    getters: {
        branches: state => {
            return state.branches;
        },
        isLoading: state => {
            return state.isLoading;
        }
    },
    actions: {

        async getBranches({ state, commit }, payload) {
            state.isLoading = true;
            Methods.checkingApiRequest(state, "getBranches");
            try {
                // const cancelToken = state.cancelSources.getBranches;
                const response = await Methods.apiCall("GET", "api/branch/get", payload, Auth.Client);
                if (response.data.success) {
                    commit("setBranches", response.data);
                }
            } catch (error) {

            }

            state.isLoading = false;
        }
    },
    mutations: {
        setBranches(state, payload) {
            state.branches = _.map(payload.data, (branch) => {
                branch.text = branch.name;
                branch.value = branch.branch_code;
                return branch;
            });
        }
    }
};
