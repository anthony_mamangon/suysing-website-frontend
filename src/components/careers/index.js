const CareerListComponent = () => ({ component: import("./careers-list"), delay: 1000 });
const CareerFormComponent = () => ({ component: import("./career-form"), delay: 1000 });

export {
    CareerFormComponent,
    CareerListComponent
};
