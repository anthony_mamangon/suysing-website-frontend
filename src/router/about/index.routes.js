import { AboutComponent, NewsComponent, NewsDetailsComponent } from "../../components/index";
import { RouterViewComponent } from "../../components/common/index";

const AboutRoutes = [
    { path: "/about", component: AboutComponent, name: "about" },
    {
        path: "/news",
        component: RouterViewComponent,
        children: [
            { path: "/", component: NewsComponent, name: "news" },
            { path: ":articleId/details", component: NewsDetailsComponent, name: "news-details", props: true }
        ]
    }

];

export default AboutRoutes;
