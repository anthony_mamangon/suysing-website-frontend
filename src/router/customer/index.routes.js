import { ApplyComponent, GrocerClubComponent, RetailAcademyComponent } from "@components/index";
import { RouterViewComponent } from "@components/common/index";

const CustomerRoutes = [
    {
        path: "/apply-now-customer",
        component: RouterViewComponent,
        children: [
            {
                path: "/",
                component: ApplyComponent,
                name: "customer-apply"
            },
            {
                path: "submitted",
                component: ApplyComponent,
                name: "customer-submitted"
            }
        ]
    },
    { path: "/luckly-grocers-club", component: GrocerClubComponent, name: "customer-grocer-club" },
    { path: "/retail-academy", component: RetailAcademyComponent, name: "customer-retail-academy" }
];

export default CustomerRoutes;
